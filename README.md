# Overview

Basic ZMQ server to include inside CCXML control

# Compilation

Source ```variables.env``` from sixdevel

e.g.
```bash
source ../sixdevel/variables.env
```

Inside the root folder of the project execute the ```cmake``` command

e.g.
```bash
cmake -DCMAKE_MODULE_PATH=$DEPLOY_DIR/share/cmake/Modules -DCMAKE_INSTALL_PREFIX=$DEPLOY_DIR -DRECURSIVE_BUILD=ON -DCMAKE_EXPORT_COMPILE_COMMANDS=ON .
```

Create binaries
```bash
make clean
make
```

This creates 2 binaries ```zmqserver``` which is a pull zmq server, and ```zmqclient```
which is a push zmq client.

# Execution

In one terminal
```bash
./zmqserver
```

In another terminal
```bash
./zmqclient
```

# Notes

 * right now the server and client dont have any kind of configuration, so if you want
 change destination ip and ports you have to edit the code and recompile with the above 
 commands
