#include <iostream>
#include <zmq.hpp>
#include <paramsCodec.hpp>
#include <paramsCodecGenericBuffer.hpp>

int main() {
    std::cout << "starting client zmq" << std::endl;

    zmq::context_t context{1};
    zmq::socket_t socketsend{context, zmq::socket_type::push};
    socketsend.connect("tcp://localhost:7050");
    zmq::socket_t socketrcv{context, zmq::socket_type::pull};
    socketrcv.connect("tcp://localhost:7058");

    const std::string data{"hello"};
    for (auto request_num = 0; request_num < 10; ++request_num) {
        ParamsCodecGenericBuffer pcBuffer;
        ParamsCodec pc;
        pc.setInteger("oper.type", 1);
        pc.setInteger("oper.idx", 1);
        pc.setInteger("oper.prmtype", 2);
        pc.setInteger("header.org.type", 1);
        pc.setString("header.session", "812739172");
        pc.setString("header.resourceId", "ijwefojwefojwefo");
        pc.writeObjectBAS(pcBuffer);
        zmq::message_t request( pcBuffer.buffer, pcBuffer.size, nullptr);
        //std::cout<< "length of paramscodec[" << pc.getBuffer()<<"]"; 
        std::cout << "sending hello[" << request_num << "]"
                  << "..." << std::endl;

        auto res{socketsend.send(request, 0)};
        if(!res) {
            std::cout << "something went really wrong" <<std::endl;
            return -1;
        }
            std::cout << "1" <<std::endl;

        zmq::message_t reply{};
         res =socketrcv.recv(&reply, 0);
            std::cout << "2" <<std::endl;

        char data[20];
        memcpy(data, (void*) reply.data(), 19);
        std::cout << "received[" << data << "]" << std::endl;
    }

    return 0;
}
