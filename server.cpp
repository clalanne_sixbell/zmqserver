
#include <chrono>
#include <iostream>
#include <thread>
#include <zmq.hpp>
#include <paramsCodec.hpp>
#include <paramsCodecGenericBuffer.hpp>

int main() {
    std::cout << "starting server!!!!" << std::endl;

    using namespace std::chrono_literals;

    zmq::context_t context{1};
    zmq::socket_t socketrcv{context, zmq::socket_type::pull};
    socketrcv.bind("tcp://127.0.0.1:7050");

    zmq::socket_t socketsend{context, zmq::socket_type::push};
    socketsend.bind("tcp://127.0.0.1:7058");

    const std::string data{"World"};
    for (;;) {
        zmq::message_t request;
        auto res{socketrcv.recv(&request, 0)};
        // std::cout << "Received " << request.to_string() << std::endl;
        ParamsCodec *newPc = new ParamsCodec;
        ParamsCodecGenericBuffer tmpBuffer;
        tmpBuffer.buffer = reinterpret_cast<char *>(request.data());
        tmpBuffer.size = request.size();
        newPc->readObjectBAS(tmpBuffer);
        tmpBuffer.buffer = NULL;

        newPc->print();
        std::cout << "Received: " << data<< std::endl;

        // work
        std::this_thread::sleep_for(1s);

        zmq::message_t response(20);
        std::cout << "1" <<std::endl;
        memcpy((void *)response.data(), "Hello from server", 19);
        std::cout << "2" <<std::endl;
        try {
            socketsend.send(response, 0);
        } catch(zmq::error_t &e) {
            std::cout << "2.5[" << e.what() << "]" <<std::endl;
        }
        std::cout << "3" <<std::endl;
    }

    return 0;
}
