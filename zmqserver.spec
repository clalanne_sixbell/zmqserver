%bcond_with devel
%global __strip $(which strip)
%define _topdir /root/rpmbuild
%define _rpmtopdir %{_topdir}
%define _builddir %{_topdir}/BUILD
%define _rpmdir %{_topdir}/RPMS
%define _sourcedir %{_topdir}/SOURCES
%define _specdir %{_topdir}/SPECS
%define _srcrpmdir %{_topdir}/SRPMS
%define _tmppath %{_topdir}/TMP


Name: zmqserver
Summary: zmqserver
Version: 0.0.0

Release: m0.g2e2495e%{?dist}
License: Commercial
Vendor: Sixbell
URL: %{buildurl}
Source: %{name}-%{version}.tar
Autoreq: %{autoreq}

Requires: SIX_paramsCodec >= 3.2.0, SIX_paramsCodec < 4.0.0



%description
zmqserver package


%if %{with devel}
%package devel
Summary: zmqserver header files
Requires: %{name} = %{version} 

Requires: SIX_paramsCodec-devel >= 3.2.0, SIX_paramsCodec-devel < 4.0.0


%description devel
zmqserver header files
%endif

%prep
rm -rf %{_builddir}/%{name}/version/%{version}
%setup -c -q -n %{name}/version/%{version}
cmake  \
  -DCMAKE_MODULE_PATH=/root/Sixbell/develop/deploy/share/cmake/Modules \
  -DCMAKE_INSTALL_PREFIX=/opt/sixlabs \
  -DBUILD_PACKAGE_FLAG:BOOL=ON \
  -DRECURSIVE_BUILD=ON \
  -DADDRESS_SANITIZER=OFF \
  -DVERSION_INFO="0.0.0" \
  -DVERSION_SUBINFO="m0-g2e2495e" \
  -DOPTIMIZER_LEVEL=1 \
  -DSTATIC_BIN=OFF \
  -DCPP_VERSION=-std=gnu++17 \
   \
   \
  .

%build
make -j4 VERBOSE=2

%install
rm -rf $RPM_BUILD_ROOT
mkdir -p $RPM_BUILD_ROOT
make install DESTDIR=$RPM_BUILD_ROOT


%pre
if ! id sixlabs > /dev/null 2>&1; 
then
	#create user
	if ! `/usr/sbin/useradd -u 266 -d /opt/sixlabs -s /sbin/nologin -M sixlabs > /dev/null 2>&1` ; #create user with userId = 266
		then #creation failed
		#create user with no uid
		/usr/sbin/useradd -d /opt/sixlabs -s /sbin/nologin -M sixlabs;
	fi;
fi;

%clean
make clean
rm -rf $RPM_BUILD_ROOT

%files 

%defattr(775,sixlabs,sixlabs,775)
/opt/sixlabs
%if %{with devel}
%exclude /opt/sixlabs/include
%exclude /opt/sixlabs/lib*/pkgconfig
%endif


%if %{with devel}
%files devel 
%defattr(755,sixlabs,sixlabs,755)
/opt/sixlabs/include
/opt/sixlabs/lib*/pkgconfig
%endif

%post
/sbin/ldconfig
#


%preun
#

%postun
/sbin/ldconfig

%changelog
*  Mon Jun 6 2022 <christian.lalanne@sixbell.com>
2e2495eb7438e49aef3f0b241423bea6994b026e
 ADD: sixbell oficial cmake

*  Mon May 30 2022 <christian.lalanne@sixbell.com>
4075363f1cdb32e511a959e0530dd6d0f81e5302
 ADD: working with pull/push paradigm

*  Mon May 30 2022 <christian.lalanne@sixbell.com>
5ba603ab7c7e446c71349cd92f06f2ed63aefc56
 ADD: proper get resource send by the client

*  Fri May 27 2022 <christian.lalanne@sixbell.com>
2a94f555038a51bb995e726316ada12487c3065e
 ADD: adding paramsCodec

*  Fri May 27 2022 <christian.lalanne@sixbell.com>
4838f8f85bff7c48f96d43139e09246f2352ed5c
 ADD: adding paramsCodec

*  Thu May 26 2022 <christian.lalanne@sixbell.com>
2f213fd39212f87843e6ebac774ee63b6df4bd11
 ADD:

*  Thu May 26 2022 <christian.lalanne@sixbell.com>
be084e68c1368fc924bd7b7570d68c845ad4722a
 ADD:

*  Thu May 26 2022 <christian.lalanne@sixbell.com>
8f0aca650eb59494cbe4df4850dc442682cc4a43
 ADD: working with old zmq

*  Wed May 25 2022 <christian.lalanne@sixbell.com>
59f38ab7c7b8f334221b658446396cdca5c2fff2
 CHG: port from 5555 to 7050

*  Wed May 25 2022 <christian.lalanne@sixbell.com>
6acce3bd5d259e9072d5e98f2775c2486a411b31
 FIX: compatible with zmq 4.3.1

*  Tue May 24 2022 <christian.lalanne@sixbell.com>
1808a9d9019e4967a23e913cbca72acbf9a76a63
 ADD: .gitignore

*  Tue May 24 2022 <christian.lalanne@sixbell.com>
6d75fb1eeda594a159325b158772badc4b48d7c9
 ADD: cmake and client, plus server and client are communicating correctly with req/resp mode

*  Tue May 24 2022 <christian.lalanne@sixbell.com>
3c2dadae8543ca0bd61c0f45c439af10b3778928
 CHG: renaming

*  Tue May 24 2022 <christian.lalanne@sixbell.com>
53ecc59dc4556d9f369ac217625410d5cc746e0d
 ADD: server

*  Mon May 23 2022 <christian.lalanne@sixbell.com>
4b0c367a32050652f2fec145c88792c3dec94ebe
 ADD: first commit

*  Mon May 23 2022 <christian.lalanne@sixbell.com>
498720e02a7f034411a86e00423f108577ab0a11
 ADD: documentation

*  Mon May 23 2022 <clalanne@sixbell.com>
f107015e3a35f51cf454b87be4f29405e5838836
 Initial commit

*  Mon May 23 2022 <clalanne@sixbell.com>
43c5550e55b54acc5bebe496a4d22ec3825bee6c
 Configure SAST in `.gitlab-ci.yml`, creating this file if it does not already exist
